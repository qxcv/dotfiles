# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# If set, the pattern "**" used in a pathname expansion context will
# match all files and zero or more directories and subdirectories.
#shopt -s globstar

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "${debian_chroot:-}" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi

# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
    xterm-color|*-256color) color_prompt=yes;;
esac

# uncomment for a colored prompt, if the terminal has the capability; turned
# off by default to not distract the user: the focus in a terminal window
# should be on the output of commands, not on the prompt
#force_color_prompt=yes

if [ -n "$force_color_prompt" ]; then
    if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
	# We have color support; assume it's compliant with Ecma-48
	# (ISO/IEC-6429). (Lack of such support is extremely rare, and such
	# a case would tend to support setf rather than setaf.)
	color_prompt=yes
    else
	color_prompt=
    fi
fi

if [ "$color_prompt" = yes ]; then
    PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '
else
    PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
fi
unset color_prompt force_color_prompt

# If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm*|rxvt*)
    PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \w\a\]$PS1"
    ;;
*)
    ;;
esac

# colored GCC warnings and errors
export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'

# Add an "alert" alias for long running commands.  Use like so:
#   sleep 10; alert
alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'

# Alias definitions.
# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.

if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi

# vvv USER CHANGES BELOW, UBUNTU 21.10 DEFAULTS ABOVE ^^^

export PATH="$HOME/.local/bin/:$PATH"
export LD_LIBRARY_PATH=$HOME/.local/lib${LD_LIBRARY_PATH:+:$LD_LIBRARY_PATH}

if [ -d /usr/local/cuda/ ]; then
    export PATH="/usr/local/cuda-8.0/bin:$PATH"
    export LD_LIBRARY_PATH="/usr/local/cuda-8.0/lib64:$LD_LIBRARY_PATH"
fi

for d in /usr/lib/nvidia-384/ ~/.mujoco/mujoco200/bin/; do
    # mujoco; these are needed by newer versions of OpenAI Gym (eg 0.10.X)
    if [ -d "$d" ]; then
        export LD_LIBRARY_PATH="$d:$LD_LIBRARY_PATH"
    fi
done

# ... if we're a virtual tty
if [ "`tty | grep ^/dev/pts/`" ]; then
    if [ -f ~/.bash_session_path ]; then
        cd "$(cat ~/.bash_session_path)"
    fi

    newcd() {
        cd "$*"
        pwd > ~/.bash_session_path
    }
    alias cd=newcd $*
fi

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    alias ls="ls --color=auto"
    alias ll="ls -lh --color=auto"
    alias la="ll -a"
    alias l=ll
    #alias dir='dir --color=auto'
    #alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi
alias sl=ls
alias rm="rm -I"
alias gvrt="gvim --remote-tab $*"
alias c="pushd"
alias d="popd"
# limit ag line width
alias ag="ag -W 120"
# -af scaletempo for pitch-independent time stretch
# -vo gl is necessary to stop mplayer from using the broken x11 driver
alias mplayer="mplayer -af scaletempo -vo gl"
tunnel() {
    usage="USAGE: tunnel <server> <port>"
    server=$1
    shift
    their_port=$1
    shift
    if [ \( -z "$server" \) -o \( -z "$their_port" \) -o \( ! \( -z "$*" \) \) ]; then
        echo -e "$usage";
        return 1
    fi
    echo "Tunnelling port $their_port on $server to localhost:$their_port (e.g. http://localhost:$their_port/)"
    ssh -N -L "${their_port}:localhost:${their_port}" "$server"
    return $@
}

export BROWSER=`which firefox`
export EDITOR=`which vim`

#Yay, vi line editing
set -o vi

# No more exclamation mark horror shows
set +H

#Append command line history every time a new prompt is encountered
# https://stackoverflow.com/questions/9457233/unlimited-bash-history
shopt -s histappend
export PROMPT_COMMAND="history -a; $PROMPT_COMMAND"
export HISTFILE=~/.bash_history_interactive
export HISTCONTROL=ignoreboth:erasedups
# Empty == unlimited!
export HISTFILESIZE=
export HISTSIZE=
export WORKON_HOME="$HOME"/.virtualenvs
_venvwrapper=/usr/share/virtualenvwrapper/virtualenvwrapper.sh
[ -e "$_venvwrapper" ] && source "$_venvwrapper"
unset _venvwrapper

# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!
__conda_setup="$('/home/sam/anaconda3/bin/conda' 'shell.bash' 'hook' 2> /dev/null)"
if [ $? -eq 0 ]; then
    eval "$__conda_setup"
else
    if [ -f "/home/sam/anaconda3/etc/profile.d/conda.sh" ]; then
        . "/home/sam/anaconda3/etc/profile.d/conda.sh"
    else
        export PATH="/home/sam/anaconda3/bin:$PATH"
    fi
fi
unset __conda_setup
# <<< conda initialize <<<
