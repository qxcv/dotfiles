silent! execute pathogen#infect()

" text editing preferences
set modeline
set tabstop=4 expandtab shiftwidth=4 softtabstop=4
set number
set enc=utf-8
set wrap tw=80
" OH GOD WHY WOULD YOU EVEN PUT TWO SPACES AFTER A FULL STOP IT IS SO
" UNUTTERABLY STUPID I DON'T EVEN WHAT IS THIS
set nojs

autocmd BufRead,BufNewFile *.py set ai tw=79
autocmd BufRead,BufNewFile *.pde set filetype=cpp
autocmd BufRead,BufNewFile *.cpp set filetype=cpp
" use tabs in C++
autocmd FileType cpp set noexpandtab

" colours
set background=dark
silent! colorscheme blackboard

filetype indent on
filetype on
filetype plugin on

" disable maliciously stupid latex idendation
autocmd FileType tex,latex set inde=
" also borked syntastic
autocmd FileType tex,latex silent! SyntasticToggleMode

set ofu=syntaxcomplete#Complete

" clear highlights
nnoremap <f3> :let @/ = ""<cr>

" tab navigation
nnoremap <C-Right> :tabnext<cr>
nnoremap <C-Left> :tabprev<cr>

" let's have ACTUAL FILE PATH COMPLETION!
set wildmode=longest,list,full
set wildmenu

" spellung
set spelllang=en
autocmd FileType rst,tex,markdown,latex set spell " tw=80 wrap

" press F11 in insert mode to enter paste mode
set pastetoggle=<f11>

" we don't do this because it pollutes our source directory :P
" let g:syntastic_c_check_header = 1
" hlint makes me needlessly infuriated
let g:syntastic_haskell_checkers=['ghc_mod']
" <3 pyflakes
let g:syntastic_python_checkers=['flake8']

" highlight trailing whitespace
" from http://chewett.co.uk/128/show-trailing-whitespace-in-vim/
set list
set listchars=tab:⇒\ ,trail:⍨

set tabpagemax=30
