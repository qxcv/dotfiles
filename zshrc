# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=100000
SAVEHIST=100000
setopt appendhistory beep extendedglob nomatch notify
unsetopt autocd
bindkey -e
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename '/home/sam/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall

# Prompt
autoload -Uz promptinit
promptinit
prompt adam2

# No history expansion!
setopt no_bang_hist

# Add local and cabal binaries to path
typeset -U path # "U" for unique---don't double-add
path=(~/.local/bin $path)

alias ls="ls --color=auto"
alias ll="ls -lh"
alias l=ll
alias la="ll -a"
alias rm="rm -i"
alias gvrt="gvim --remote-tab $*"
alias c="pushd"
alias d="popd"
# -af scaletempo for pitch-independent time stretch
# -vo gl is necessary to stop mplayer from using the broken x11 driver
alias mplayer="mplayer -af scaletempo -vo gl"
alias palossh="ssh paloalto.cecs.anu.edu.au -t tmux a"

export BROWSER=`which firefox`
export EDITOR=`which vim`
export GOPATH=~/.go

# ... if we're a virtual tty
if [ "`tty | grep \^/dev/pts/`" ]; then
    if [ -f ~/.zsh_session_path ]; then
        cd "$(cat ~/.zsh_session_path)"
    fi

    newcd() {
        cd "$*"
        pwd > ~/.zsh_session_path
    }
    alias cd=newcd $*
fi

# zsh has its own annoyances
# https://pilif.github.io/2004/10/delete-key-in-zsh/
bindkey    "^[[3~"          delete-char
bindkey    "^[3;5~"         delete-char
