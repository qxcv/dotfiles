#!/bin/bash

# Somewhat brittle script to back up my laptop.

set -euo pipefail

BACKUP_HOST="${BACKUP_HOST:-$HOSTNAME}"
repo_name="b2:restic-laptop-backup:machines/$BACKUP_HOST"
backup_dirs=(~/Sync/ ~/repos/notes-git/ ~/repos/password-store/ ~/Books/)
export RESTIC_PASSWORD_COMMAND="pass restic/$BACKUP_HOST"
source ~/.config/backblaze

usage() {
    echo "USAGE: $0 {init,backup,check,cmd}"
    exit 1
}

assert_no_more_args() {
    if [ "$#" -gt 0 ]; then
        usage
    fi
}

init() {
    assert_no_more_args

    echo "Will initialise repo $repo_name"
    restic -r "$repo_name" init

    echo "Init done. Doing a check."
    restic -r "$repo_name" check
}

backup() {
    assert_no_more_args

    echo "Backing up the following directories: ${backup_dirs[@]}"
    restic -r "$repo_name" backup "${backup_dirs[@]}"

    echo "Backup done. Doing a check"
    check
}

check() {
    assert_no_more_args

    echo "Checking repo ${repo_name}"
    restic -r "$repo_name" check
}

run_restic_cmd() {
    echo "Running restic with args: $@"
    restic -r "$repo_name" $@
}

case "$1" in
    init)
        shift
        init $@
        ;;
    backup)
        shift
        backup $@
        ;;
    check)
        shift
        check $@
        ;;
    cmd)
        shift
        # run an arbitrary command
        run_restic_cmd $@
        ;;
    *)
        usage
        ;;
esac
