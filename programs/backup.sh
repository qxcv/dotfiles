#!/bin/bash

set -e
PWSTORE_DEST="password-store"

dest="$1"
if [[ ( -z "$dest" ) || (-n "$2") ]]; then
    echo "USAGE: $0 <destination>"
    exit 1
fi

echo "Making destination directory '$dest'"
mkdir -pv "$dest"
pushd "$dest"

# handle password store
if [[ ! ( -e "$PWSTORE_DEST" ) ]]; then
    echo "Cloning password store into '$PWSTORE_DEST'"
    git clone git@bitbucket.org:qxcv/password-store.git "$PWSTORE_DEST"
else
    echo "Updating password store at '$PWSTORE_DEST'"
    pushd "$PWSTORE_DEST"
    git pull
    popd
fi

# handle encrypted private keys
echo "Writing GPG keys to file (will ask for password)"
today="$(date -I)"
pubkey_dest="public-keys-${today}.gpg"
privkey_dest="encrypted-private-keys-${today}.gpg"
gpg -a --export --output "$pubkey_dest"
gpg -a --export-secret-keys --output "$privkey_dest"
echo "Done. To reimport, use the following commands:"
echo "  gpg --allow-secret-key-import --import $privkey_dest"
echo "  gpg --import $pubkey_dest"

# back up ~/Sync
sync_dest="sync-backup-${today}.tar.gz.gpg"
echo "Backing up ~/Sync to '$sync_dest'"
tar czfv - "$HOME/Sync/" | gpg -e --sign -r sam@qxcv.net -o "$sync_dest"
echo "Done. To decrypt on the other end, use:"
echo "  gpg -d $sync_dest | tar xf"
